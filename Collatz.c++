// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Collatz.h"

using namespace std;

// -------
// globals
// -------

int cache [100000] = {};

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    // pre: 0 < i, j < 1,000,000
    assert(0 < i && 0 < j && i < 1000000 && j < 1000000);

    // <your code>
    int high, low;
    if (i > j) {
        high = i;
        low = j;
    }
    else {
        high = j;
        low = i;
    }
    int maxCyc = -1;
    for (int cur = low; cur <= high; ++cur) {
        unsigned long long col = cur;
        int curCyc = 1;
        while (col > 1) {
            // if small enough to be in cache && has cached value
            if (col < 100000 && cache[col] != 0) {
                // subtract one so first is not counted twice
                curCyc += cache[col] - 1;
                break;
            }
            // odd
            if (col % 2) {
                // multiplies by 3 then adds one
                col = (col << 1) + col + 1;
                // can always divide the resulting even number
                col >>= 1;
                // must add for even cycle completed
                ++curCyc;
            }
            // even
            else {
                // divides by 2
                col >>= 1;
            }
            ++curCyc;
        }
        // has not been cached before
        if (cur < 100000 && cache[cur] == 0) {
            cache[cur] = curCyc;
        }
        if (curCyc > maxCyc) {
            maxCyc = curCyc;
        }
    }

    // post: max cycle length at least 1
    assert(maxCyc > 0);

    return maxCyc;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
